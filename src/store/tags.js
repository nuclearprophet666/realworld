import { fetch } from "@/api/tags";

const state = {
  tags: null,
  isLoading: null,
  errors: null,
}

const getters = {
  tags: (state) => state.tags,
  isLoading: (state) => state.isLoading,
  errors: (state) => state.errors,
}

const mutations = {
  fetchStart(state) {
    state.isLoading = true;
    state.errors = null;
  },
  fetchSuccess(state, data) {
    state.isLoading = false;
    state.tags = data;
  },
  fetchFailure(state, errors) {
    state.isLoading = false;
    state.errors = errors;
  }
}

const actions = {
  fetch: async({commit}) => {
    commit('fetchStart')
    try {
      const {data} = await fetch()
      commit('fetchSuccess', data.tags)
    } catch({ response }) {
      const errors = response.data.errors
      commit('fetchFailure', errors)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
