const state = {
  navigation: [
    {
      path: "/new",
      name: "NewArticle",
      text: "New Article",
      hideOnLoggedIn: false,
      exact: true
    },
    {
      path: "/settings",
      name: "Settings",
      text: "Settings",
      hideOnLoggedIn: false,
      exact: true
    },
    {
      path: "/login",
      name: "Login",
      text: "Sign In",
      hideOnLoggedIn: true,
      exact: true
    },
    {
      path: "/register",
      name: "Register",
      text: "Sign Up",
      hideOnLoggedIn: true,
      exact: true
    },
  ]
};

const getters = {
  navigation: state => state.navigation,
  filteredNavigation: (state, getters, rootState, rootGetters) => {
    return state.navigation.filter(item => {
      if (Object.hasOwnProperty.call(item, "hideOnLoggedIn")) {
        return item.hideOnLoggedIn === rootGetters["auth/isLoggedOut"];
      }
      return true;
    });
  }
};

export default {
  namespaced: true,
  state,
  getters
};
