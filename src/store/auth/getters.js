export default {
  isSubmiting: state => state.isSubmiting,
  isLoggedIn: state => Boolean(state.currentUser),
  isLoggedOut: (state, getters) => !getters.isLoggedIn,
  userChecked: state => state.userChecked,
  currentUser: state => state.currentUser,
  errors: state => {
    if (state.errors) {
      const messages = [];
      Object.keys(state.errors).map(key => {
        state.errors[key].map(i => {
          return messages.push(`${key} ${i}`);
        });
      });
      return messages;
    } else {
      return null;
    }
  }
};
