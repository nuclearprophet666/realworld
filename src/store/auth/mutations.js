export default {
  authStart(state) {
    state.isSubmiting = true;
    state.errors = null;
  },
  authSuccess(state, payload) {
    state.isSubmiting = false;
    state.currentUser = payload;
  },
  authFailure(state, payload) {
    state.isSubmiting = false;
    state.errors = payload;
  },
  clearErrors(state) {
    state.errors = null;
  },
  logout(state) {
    state.currentUser = null;
  },
  userCheck(state) {
    state.userChecked = true;
  }
};
