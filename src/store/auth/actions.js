import { register, login, getCurrentUser } from "@/api/auth";
import { localStorageSet } from "@/helpers/localStorage";

export default {
  register: async ({ commit }, credentials) => {
    commit("authStart");
    try {
      const { data } = await register(credentials);
      localStorageSet("authToken", data.user.token);
      commit("authSuccess", data.user);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("authFailure", errors);
    }
  },
  login: async ({ commit }, credentials) => {
    commit("authStart");
    try {
      const { data } = await login(credentials);
      localStorageSet("authToken", data.user.token);
      commit("authSuccess", data.user);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("authFailure", errors);
    }
  },
  getCurrentUser: async ({ commit }) => {
    commit("authStart");
    try {
      const { data } = await getCurrentUser();
      commit("authSuccess", data.user);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("authFailure", errors);
    }
  }
};
