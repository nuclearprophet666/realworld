import { fetchGlobal, fetchYours, fetchByTag, fetchCurrent, add } from "@/api/articles";

const state = {
  isLoading: false,
  articles: null,
  articlesCount: null,
  current: null,
  errors: null,
  routes: [
    {
      path: "/",
      name: "GlobalFeed",
      text: "Global Feed",
      exact: true,
    },
    {
      path: "/feed",
      name: "YourFeed",
      text: "Your Feed",
      exact: true,
      hideOnLoggedOut: true
    }
  ]
};

const getters = {
  articles: state => state.articles,
  articlesCount: state => state.articlesCount,
  current: state => state.current,
  isLoading: state => state.isLoading,
  errors: state => state.errors,
  routes: state => state.routes,
  filteredRoutes: (state, getters, rootState, rootGetters) => {
    return state.routes.filter(item => {
      if (Object.hasOwnProperty.call(item, "hideOnLoggedOut")) {
        return item.hideOnLoggedOut !== rootGetters["auth/isLoggedOut"];
      }
      return true;
    });
  }
};

const mutations = {
  fetchStart(state) {
    state.isLoading = true;
    state.errors = null;
  },
  fetchSuccess(state, data) {
    state.isLoading = false;
    state.articles = data.articles;
    state.articlesCount = data.articlesCount
  },
  fetchFailure(state, errors) {
    state.isLoading = false;
    state.errors = errors;
  },
  currentStart(state) {
    state.isLoading = true;
    state.errors = null;
  },
  currentSuccess(state, data) {
    state.isLoading = false;
    state.current = data.article;
  },
  currentFailure(state, errors) {
    state.isLoading = false;
    state.errors = errors;
  },
};

const actions = {
  fetchGlobal: async ({ commit }, payload) => {
    commit("fetchStart");
    try {
      const { limit, offset } = payload;
      const { data } = await fetchGlobal(limit, offset);
      commit("fetchSuccess", data);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("fetchFailure", errors);
    }
  },
  fetchYours: async ({ commit }, payload) => {
    commit("fetchStart");
    try {
      const { limit, offset } = payload;
      const { data } = await fetchYours(limit, offset);
      commit("fetchSuccess", data);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("fetchFailure", errors);
    }
  },
  fetchByTag: async ({ commit }, payload) => {
    commit("fetchStart");
    try {
      const { limit, offset, tag } = payload;
      const { data } = await fetchByTag(limit, offset, tag);
      commit("fetchSuccess", data);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("fetchFailure", errors);
    }
  },
  fetchCurrent: async ({ commit }, payload) => {
    commit("currentStart");
    try {
      const { data } = await fetchCurrent(payload);
      commit("currentSuccess", data);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("currentFailure", errors);
    }
  },
  add: async ({ commit }, payload) => {
    commit("fetchStart");
    try {
      const { data } = await add(payload);
      commit("fetchSuccess", data);
    } catch ({ response }) {
      const errors = response.data.errors;
      commit("fetchFailure", errors);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
