import Vue from "vue";
import Vuex from "vuex";

import auth from "@/store/auth";
import navigation from "@/store/navigation";
import articles from "@/store/articles";
import tags from "@/store/tags";

Vue.use(Vuex);

export default new Vuex.Store({
  namespaced: true,
  modules: {
    auth,
    navigation,
    articles,
    tags
  }
});
