import Vue from "vue";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store";

import checkUser from "@/helpers/checkUser";
import vuetify from "./plugins/vuetify";

import "@/assets/scss/app.scss";

Vue.config.productionTip = false;

const init = async () => {
  await checkUser();
  new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount("#app");
};

init();
