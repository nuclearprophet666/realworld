import { localStorageGet } from "@/helpers/localStorage";
import store from "@/store";

export default async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters["auth/isLoggedOut"]) {
      const token = localStorageGet("authToken");
      if (token) {
        await store.dispatch("auth/getCurrentUser");
        next();
      }
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
};
