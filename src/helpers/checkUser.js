import { localStorageGet } from "@/helpers/localStorage";
import store from "@/store";

export default async () => {
  const token = localStorageGet("authToken");
  if (token) {
    await store.dispatch("auth/getCurrentUser");
  }
  store.commit("auth/userCheck");
};
