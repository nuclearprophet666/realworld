import Home from "@/views/Home";
import Login from "@/views/Login";
import Register from "@/views/Register";
import NewArticle from "@/views/NewArticle";
import Settings from "@/views/Settings";
import User from "@/views/User";
import Article from "@/views/Article";
// import Feed from "@/views/Feed";
import YourFeed from "@/views/YourFeed";
import GlobalFeed from "@/views/GlobalFeed";
import TagFeed from "@/views/TagFeed";
import NotFound from "@/views/NotFound";

export default [
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      requiresAuth: false
    }
    // () => import(/* webpackChunkName: "register" */ "../views/Register.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: "/new",
    name: "NewArticle",
    component: NewArticle,
    // beforeEnter: isAuth,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/user",
    name: "User",
    component: User,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/articles/:slug",
    name: "Article",
    component: Article,
    // beforeEnter: isAuth,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    component: Home,
    props: true,
    // beforeEnter: fetchArticles,
    children: [
      {
        path: '/feed',
        name: 'YourFeed',
        component: YourFeed,
        meta: {
          requiresAuth: true
        },
      },
      {
        path: '/tags/:tag',
        name: 'TagFeed',
        component: TagFeed,
        meta: {
          requiresAuth: false
        },
      },
      { 
        path: '/',
        name: 'GlobalFeed',
        component: GlobalFeed,
        meta: {
          requiresAuth: false
        },
      },
    ]
  },
  {
    path: "*",
    name: "NotFound",
    component: NotFound
  }
];