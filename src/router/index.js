import Vue from "vue";
import VueRouter from "vue-router";

import routes from '@/router/routes'

import checkAuth from "@/helpers/router/checkAuth";
// import fetchArticles from '@/helpers/router/fetchArticles'

Vue.use(VueRouter);



const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
});

router.beforeEach(checkAuth);

export default router;
