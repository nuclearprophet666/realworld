import api from "@/api/axios";

export const fetchGlobal = async (limit, offset) => {
  return await api.get("/articles", { params: { limit, offset } });
};

export const fetchYours = async (limit, offset) => {
  return await api.get("/articles/feed", { params: { limit, offset } });
};

export const fetchByTag = async (limit, offset) => {
  return await api.get("/articles/feed", { params: { limit, offset } });
};

export const fetchCurrent = async (slug) => {
  return await api.get(`/articles/${slug}`);
};

export const add = async (article) => {
  return await api.post("/articles", article );
};
