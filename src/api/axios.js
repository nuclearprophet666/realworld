import axios from "axios";
import { localStorageGet } from "@/helpers/localStorage";

const axiosParams = {
  baseURL: "https://conduit.productionready.io/api"
};

const axiosInstance = axios.create(axiosParams);

axiosInstance.interceptors.request.use(config => {
  const token = localStorageGet("authToken");
  config.headers.Authorization = token ? `Token ${token}` : "";
  return config;
});

const api = axios => {
  return {
    get: (url, config) => axios.get(url, config),
    post: (url, body, config) => axios.post(url, body, config)
  };
};

export default api(axiosInstance);
