import api from "@/api/axios";

export const fetch = async () => {
  return await api.get("/tags");
};