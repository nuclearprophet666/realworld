import api from "@/api/axios";

export const register = async credentials => {
  return await api.post("users", { user: credentials });
};

export const login = async credentials => {
  return await api.post("users/login", { user: credentials });
};

export const getCurrentUser = async () => {
  return await api.get("user");
};
